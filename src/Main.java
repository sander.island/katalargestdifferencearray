public class Main {
    public static void main(String[] args) {
        int[] numbers1 = {2, 3, 1, 7, 9, 5, 11, 3, 5};
        int[] numbers2 = {3, 4, 2, 6, 9, 10, 1, 6, 7};

        System.out.println("Numbers1: ");
        calculate(numbers1);
        System.out.println("-----------------");
        System.out.println("Numbers2: ");
        calculate(numbers2);
    }

    public static void calculate(int[] numbers){
        int smallest = 0;
        int largest = 0;
        int smallestIndex = 0;
        int largestIndex = 0;


        // Find Largest
        for (int i = 0; i < numbers.length; i++){
            if (i == 0){
                smallest = numbers[0];
                largest = numbers[0];
            }

            if (largest < numbers[i]){
                largest = numbers[i];
                largestIndex = i;
            }
        }
        // Find smallest
        for (int i = 0; i < largestIndex; i++){
            if (smallest > numbers[i]){
                smallest = numbers[i];
                smallestIndex = i;
            }
        }

        int difference = largest - smallest;
        System.out.println("Smallest: " + smallest);
        System.out.println("Smallest Index: " + smallestIndex);
        System.out.println("Largest: " + largest);
        System.out.println("Largest Index: " + largestIndex);
        System.out.println("Difference: " + (difference));

        System.out.println("Pairs:");
        int pairs = 0;
        for (int i = 0; i < numbers.length; i++){
            for (int j = 0; j < numbers.length; j++){
                if (numbers[i] + numbers[j] == difference && i < j){
                    pairs ++;
                    System.out.println(numbers[i] + "(" + i + ") + " + numbers[j] + "(" + j + ") = " + (numbers[i] + numbers[j]));
                }
            }
        }
        System.out.println("Pairs giving the sum of " + difference + ": " + pairs);
    }
}